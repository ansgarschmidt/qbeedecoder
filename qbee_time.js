#
#Preprocessor used in kaitai struct to convert time in qbee sat
#
# Ansgar Schmidt, DM1AS (dm1as@23-5.eu)
# Based on the elfin_pp script
# Usage in 'kaitai struct':
#
# types:
#   preprocessor:
#     seq:
#       - id: databuf
#         process: qbee_time

/*
 * to use this in kaitai WebIDE remove comments:
 */
//localStorage.setItem("userTypes", `
class QbeeTime {

    constructor(_key) {
        this.key = _key;
    }

    _read() {
        this._io = {};
        this._debug = {};
    }

    decode(bindata)
    {
        var start_date = new Date('2000-01-01 00:00:00');
        var seconds = bindata[0] + (bindata[1] * 256) + (bindata[2] * 65536) + (bindata[3] * 16777216);
        var utcdate = new Date(start_date.getTime() + (seconds * 1000));
        return utcdate.toString();
    }

} this.QbeeTime = QbeeTime;
//`); 
