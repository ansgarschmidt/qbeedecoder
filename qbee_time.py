"""
Preprocessor used in kaitai struct to convert time in qbee sat
"""
#
# Ansgar Schmidt, DM1AS (dm1as@23-5.eu)
# Based on the elfin_pp script
# Usage in 'kaitai struct':
#
# types:
#   preprocessor:
#     seq:
#       - id: databuf
#         process: qbee_time

import struct
import binascii
import datetime

class QbeeTime(object):  # pylint: disable=too-few-public-methods
    """
    Preprocessor used in kaitai struct to convert time in qbee sat
    """
    def decode(self, bindata):  # pylint: disable=no-self-use
        """
        Preprocessor used in kaitai struct to convert time in qbee sat
        """
        start_date = datetime.datetime(2000, 1, 1, 0, 0) # 1. Januar 2000 00:00:00Z
        seconds = struct.unpack('<I', bindata)[0] # LSB 4 byte
        delta = datetime.timedelta(seconds=seconds)
        utcdate = start_date + delta
        return utcdate.strftime("%Y-%m-%dT%H:%H:%SZ")


if __name__ == '__main__':
    # some examples
    DEC = QbeeTime()
    #time: 2011-01-31T07:20:46Z (0x14d91f4e)
    print(binascii.hexlify(binascii.unhexlify('4e1fd914')))
    print(DEC.decode(binascii.unhexlify('4e1fd914')))
